package no.ntnu.imt3281.sudoku;

import org.json.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Random;

/**
 * Class for building the sudoku board from the board.json file
 */
public class SudokuBuilder {

    protected int[][] board = null;
    int max = 5, min = 1;
    private String jsonFile = "board.json";

    public int[][] build(int[][] board) {

        this.board = board;

        try {

            String jsonPath = Objects.requireNonNull(Thread.currentThread().getContextClassLoader().getResource(jsonFile)).getPath();

            Reader in = new InputStreamReader(new FileInputStream(jsonPath), StandardCharsets.UTF_8);

            JSONArray numbers = new JSONArray(new JSONTokener(in));

            in.close();

            for (int i = 0; i < numbers.length(); i++) {

                JSONArray row = numbers.optJSONArray(i);

                for (int j = 0; j < row.length(); j++) {

                    board[i][j] = row.optInt(j);

                }

            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;

        }


        Random rand = new Random();

        switch (rand.nextInt(max + 1 - min) + min) {
            case 1:
                randomizeNumbers(9);
                break;
            case 2:
                board = mirrorRed();
                break;
            case 3:
                board = mirrorBlue();
                break;
            case 4:
                board = mirrorHorizontally();
                break;
            case 5:
                board = mirrorVertical();
                break;
        }
        return board;
    }

    /**
     * Switches all occurances of two random numbers on the board
     *
     * @param noOfRandomizes takes a number of times to run the randomizer
     */
    protected void randomizeNumbers(int noOfRandomizes) {

        Random rand = new Random();
        int numbToChange1 = 0;
        int numbToChange2 = 0;
        for (int i = 0; i < noOfRandomizes; i++) {  //How many times to run the randomizer
            numbToChange1 = rand.nextInt(9) + 1;
            do {
                numbToChange2 = rand.nextInt(9) + 1;
            } while (numbToChange1 == numbToChange2); //Make sure the two numbers are not the same

            for (int col = 0; col < Sudoku.NO_OF_ROWS; col++) {
                for (int row = 0; row < Sudoku.NO_OF_ROWS; row++) {
                    if (board[row][col] == numbToChange1) {
                        board[row][col] = numbToChange2;
                    } else if (board[row][col] == numbToChange2) {
                        board[row][col] = numbToChange1;
                    }
                }

            }
        }
        System.out.println("Randomize");
    }

    /**
     * The function creates a new 2D int array of the same size as int[][] board but mirrors it across the red line,
     * switching the numbers in the top-left with the bottom-right.
     *
     * @return int[][] newBoard    A new 2D int array mirrored from the top-left to the bottom-right.
     * @see int[][] board
     */
    protected int[][] mirrorRed() {

        int[][] newBoard = new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS];

        for (int col = Sudoku.NO_OF_ROWS - 1; col >= 0; col--) {

            for (int row = Sudoku.NO_OF_ROWS - 1; row >= 0; row--) {

                newBoard[Sudoku.NO_OF_ROWS - row - 1][Sudoku.NO_OF_ROWS - col - 1] = board[col][row];

            }

        }
        System.out.println("Mirror Red");
        return newBoard;

    }

    /**
     * The function creates a new 2D int array of the same size as int[][] board but mirrors it across the blue line,
     * switching the numbers in the top-right with the bottom-left.
     *
     * @return int[][] newBoard    A new 2D int array mirrored from the top-right to the bottom-left.
     * @see int[][] board
     */
    protected int[][] mirrorBlue() {

        int[][] newBoard = new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS];

        for (int col = 0; col < Sudoku.NO_OF_ROWS; col++) {

            for (int row = 0; row < Sudoku.NO_OF_ROWS; row++) {

                newBoard[row][col] = board[col][row];

            }

        }
        System.out.println("Mirror Blue");
        return newBoard;

    }

    /**
     * The function creates a new 2D int array of the same size as int[][] board but mirrors it horizontally,
     * switching the numbers in the left rows with the right rows.
     *
     * @return int[][] newBoard    A new 2D int array mirrored horizontally.
     * @see int[][] board
     */
    protected int[][] mirrorHorizontally() {

        int[][] newBoard = new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS];

        for (int col = Sudoku.NO_OF_ROWS - 1; col >= 0; col--) {

            for (int row = Sudoku.NO_OF_ROWS - 1; row >= 0; row--) {

                newBoard[Sudoku.NO_OF_ROWS - row - 1][Sudoku.NO_OF_ROWS - col - 1] = board[Sudoku.NO_OF_ROWS - row - 1][col];

            }

        }
        System.out.println("Mirror Horizontally");
        return newBoard;

    }

    /**
     * The function creates a new 2D int array of the same size as int[][] board but mirrors it vertically,
     * switching the numbers in the top rows with the bottom rows.
     *
     * @return int[][] newBoard    A new 2D int array mirrored vertically.
     * @see int[][] board
     */
    protected int[][] mirrorVertical() {

        int[][] newBoard = new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS];

        for (int col = 0; col < Sudoku.NO_OF_ROWS; col++) {

            for (int row = Sudoku.NO_OF_ROWS - 1; row >= 0; row--) {

                newBoard[Sudoku.NO_OF_ROWS - row - 1][col] = board[row][col];

            }

        }
        System.out.println("Mirror Vertically");
        return newBoard;

    }

}
