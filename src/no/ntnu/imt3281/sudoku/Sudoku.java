package no.ntnu.imt3281.sudoku;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * JavaFX App
 */
public class Sudoku extends Application {

    static final int NO_OF_ROWS = 9;
    public int[][] board = new int[NO_OF_ROWS][NO_OF_ROWS];
    protected GridPane[][] boardGrids = new GridPane[3][3];
    private SudokuBuilder builder = new SudokuBuilder();
    private Stage s;

    private TextArea textArea;

    private ResourceBundle messages;

    /**
     * Build a new board.
     */
    public Sudoku() {

        newBoard();

    }

    @Override
    public void start(Stage stage) {

        s = stage;
        updateScene();

    }

    /**
     * Updating the shown scene when either button for creating a new game is pushed.
     */
    private void updateScene() {

        s.setScene(new Scene(buildGraphics()));
        s.show();

    }

    /**
     * This function is called in the start(..) function and serves the purpose of creating the scene to be set
     * in the stage, and used in new Scene(buildGraphics()). It creates the sudoku board from a json file
     * which is then placed withing GridPanes withing the returned BorderPane.
     *
     * @return BorderPane  Programmatically created scene view.
     */
    public BorderPane buildGraphics() {

        BorderPane borderPane = new BorderPane();
        BorderPane bottomPane = new BorderPane();
        GridPane mainGrid = new GridPane();

        textArea = new TextArea();
        textArea.setStyle("-fx-pref-width: 8.5em; -fx-pref-height: 3.4em;");
        textArea.setEditable(false);

        Button easy = new Button();
        Button medium = new Button();

        messages = ResourceBundle.getBundle("i18n.Messages", Locale.getDefault());

        easy.setText(messages.getString("easyButton"));
        medium.setText(messages.getString("mediumButton"));


        /*
            Når brukeren trykker på en av knappene for nytt spill lages det et nytt brett,
            grafikken oppdateres og brettet oppdateres for å vise den nye grafikken.
            Samme funksjon for easy og medium.
         */
        easy.setOnAction(actionEvent -> {
                    newBoard();
                    buildGraphics();
                    updateScene();
                }
        );

        medium.setOnAction(actionEvent -> {
                    newBoard();
                    buildGraphics();
                    updateScene();
                }
        );

        bottomPane.setLeft(easy);
        bottomPane.setCenter(textArea);
        bottomPane.setRight(medium);

        borderPane.setCenter(mainGrid);
        borderPane.setBottom(bottomPane);

        for (int blockColumn = 0; blockColumn < 3; blockColumn++) {

            for (int blockRow = 0; blockRow < 3; blockRow++) {

                GridPane grid = new GridPane();
                boardGrids[blockColumn][blockRow] = grid;

                final int c = blockColumn, r = blockRow;

                grid.setStyle("-fx-background-color: black; -fx-background-insets: 0, 2; -fx-padding: 2;");

                for (int column = 0; column < 3; column++) {

                    for (int row = 0; row < 3; row++) {

                        int ic = column, ir = row;
                        int n = board[row + blockRow * 3][column + blockColumn * 3];

                        TextField textField;

                        // Dersom tallet leses inn fra JSON, og det skal låses.
                        if (n != -1) {

                            textField = new TextField("" + n);
                            textField.setStyle("-fx-control-inner-background: gray; -fx-pref-width: 3.5em; -fx-pref-height: 3em;");
                            lockField(textField);

                        } else {

                            textField = new TextField("");
                            textField.setStyle("-fx-pref-width: 3.5em; -fx-pref-height: 3em;");
                            // Listener for når brukeren skriver noe i et textField.
                            textField.textProperty().addListener((observable, oldValue, newValue) -> {

                                // Sjekk om innskrevet verdi er et tall.
                                if (newValue.matches("^-?\\d+$")) {

                                    int val = Integer.parseInt(newValue);

                                    // Sjekker om tallet høyere enn 0 og lavere enn 10.
                                    if (val > 0 && val < 10) {

                                        /*
                                            Sjekker om nye nummer skrevet er lovlig i gjeldende 3x3 rute
                                            når et TextField blir oppdatert.
                                         */
                                        Boolean boxValid = checkBoxValid(c, r);
                                        Boolean colValid = checkColValid(c, ic);
                                        Boolean rowValid = checkRowValid(r, ir);

                                        if (boxValid && colValid && rowValid) {
                                            board[ir + r * 3][ic + c * 3] = val;

                                        } else {

                                            textField.setStyle("-fx-control-inner-background: red; -fx-pref-width: 3.5em; -fx-pref-height: 3em;");

                                        }

                                    } else {

                                        textField.setText("");
                                        board[ir + r * 3][ic + c * 3] = -1;
                                        textField.setStyle("-fx-pref-width: 3.5em; -fx-pref-height: 3em;");

                                    }

                                    // Dersom noe annet ugyldig er skrevet inn eller om valget slettes
                                } else {

                                    // Feltet settes blankt.
                                    textField.setText("");
                                    board[ir + r * 3][ic + c * 3] = -1;
                                    textField.setStyle("-fx-pref-width: 3.5em; -fx-pref-height: 3em;");

                                }

                                if (checkComplete()) {

                                    finished();

                                }

                            });

                        }

                        textField.setAlignment(Pos.CENTER);

                        GridPane.setConstraints(textField, column, row);
                        grid.getChildren().add(textField);

                    }

                }

                GridPane.setConstraints(grid, blockColumn, blockRow);
                mainGrid.getChildren().add(grid);

            }

        }

        return borderPane;

    }

    /**
     * Called after a user has written something in one of the open text fields. Loops through the 9x9 array
     * containing validly placed numbers. Returns false if one area is empty or equals to -1.
     * @return  Boolean True if all spots on the board are filled and valid.
     */
    private Boolean checkComplete() {

        for (int col = 0; col < NO_OF_ROWS; col++) {

            for (int row = 0; row < NO_OF_ROWS; row++) {

                if (board[col][row] < 1) {

                    return false;

                }

            }

        }

        return true;

    }

    /**
     * Shows the user a short text when the game is completed.
     */
    private void finished() {

        textArea.setText(messages.getString("gameWonMessage"));

    }

    /**
     * This method is added in each textField Listener, and it checks if a number exists in the column for the
     * entire board when a number is written in a textField.
     *
     * @param boxCol The relevant column value of the 3x3 box which the number entered is placed in.
     * @param numCol The column value for the number entered number withing the 3x3 box.
     * @return Boolean  Set to false if number already exists in the given column.
     */
    protected Boolean checkColValid(int boxCol, int numCol) {

        List<String> numbers = new ArrayList<>();

        for (int boxRow = 0; boxRow < 3; boxRow++) {

            Node[][] gridPaneNodes = getGridNodes(boardGrids[boxCol][boxRow]);

            // Henter hver node i kolonnen, med loop gjennom verdien for raden.
            for (int row = 0; row < 3; row++) {

                Node node = gridPaneNodes[numCol][row];
                String n = ((TextField) node).getText();

                if (n != null && !n.equals("")) {

                    if (numbers.contains(n)) {

                        return false;

                    } else {

                        numbers.add(n);

                    }

                }

            }

        }

        return true;

    }

    /**
     * This method is added in each textField Listener, and it checks if a number exists in the row for the
     * entire board when a number is written in a textField.
     *
     * @param boxRow The relevant row value of the 3x3 box which the number entered is placed in.
     * @param numRow The row value for the number entered number withing the 3x3 box.
     * @return Boolean  Set to false if number already exists in the given row.
     */
    protected Boolean checkRowValid(int boxRow, int numRow) {

        List<String> numbers = new ArrayList<>();

        for (int boxCol = 0; boxCol < 3; boxCol++) {

            Node[][] gridPaneNodes = getGridNodes(boardGrids[boxCol][boxRow]);

            for (int col = 0; col < 3; col++) {

                // Henter hver node i raden, med loop gjennom kolonneverdien.
                Node node = gridPaneNodes[col][numRow];
                String n = ((TextField) node).getText();

                if (n != null && !n.equals("")) {

                    if (numbers.contains(n)) {

                        return false;

                    } else {

                        numbers.add(n);

                    }

                }

            }

        }

        return true;

    }

    /**
     * This method is added in each textField Listener, and it checks if a number already exists in the 3x3 box.
     *
     * @param c The column value of the 3x3 box which the number entered is placed in.
     * @param r The row value of the 3x3 box which the number entered is placed in.
     * @return Boolean  Set to false if number already exists in the relevant 3x3 box.
     */
    protected Boolean checkBoxValid(int c, int r) {

        // Henter riktig grid fra 3x3 2D GridPane array.
        Node[][] gridPaneNodes = getGridNodes(boardGrids[c][r]);

        List<String> numbers = new ArrayList<>();

        for (int col = 0; col < 3; col++) {

            for (int row = 0; row < 3; row++) {

                /*
                    Henter noden i nåværende posisjon i loopen og henter verdien i TextField.
                 */
                Node node = gridPaneNodes[row][col];
                String n = ((TextField) node).getText();

                /*
                    Formaterer output for debugging. Skriver 3x3 grid til konsollen.
                 */
                System.out.print(n);
                if (row != 2) {
                    System.out.print("  ");
                } else {
                    System.out.print(" \n");
                }

                if (n != null && !n.equals("")) {

                    if (numbers.contains(n)) {

                        return false;

                    } else {

                        numbers.add(n);

                    }

                }

            }

        }

        System.out.print("\n");

        return true;

    }

    /**
     * Creates a new 2x2 array of nodes containing the children from the given GridPane.
     *
     * @param gridPane A specific 3x3 board gotten from boardGrids.
     * @return Node[][] Array of nodes, children of param.
     */
    protected Node[][] getGridNodes(GridPane gridPane) {

        Node[][] gridPaneNodes = new Node[3][3];

        for (Node child : gridPane.getChildren()) {

            Integer column = GridPane.getColumnIndex(child);
            Integer row = GridPane.getRowIndex(child);

            if (column != null && row != null) {

                gridPaneNodes[column][row] = child;

            }

        }

        return gridPaneNodes;

    }

    /**
     * Calling SudokuBuilder build function to create a new game board when Create new game button is pushed.
     */
    private void newBoard() {

        board = builder.build(board);
        printBoard();

    }

    /**
     * Printing 9x9 board to console for debugging purposes.
     */
    protected void printBoard() {

        for (int col = 0; col < NO_OF_ROWS; col++) {

            for (int row = 0; row < NO_OF_ROWS; row++) {

                System.out.print(board[col][row] + ", ");

            }

            System.out.print("\n");

        }

    }

    /**
     * Function called for testing purposes, that checks whether or not the textField is editable.
     * @return  Boolean     True if textField is editable. False, locked, if it got a number from the JSON field.
     */
    protected Boolean checkEditable(int boxCol, int boxRow, int col, int row) {

        return ((TextField) getGridNodes(boardGrids[boxCol][boxRow])[col][row]).isEditable();

    }

    /**
     * Function called for testing purposes, returning the value of the given textField.
     * @return  String      The number written in the textField.
     */
    protected String getNode(int boxCol, int boxRow, int col, int row) {

        return ((TextField) getGridNodes(boardGrids[boxCol][boxRow])[col][row]).getText();

    }

    /**
     * Function called for testing purposes, defining the value inside the given textField.
     */
    protected void setNode(int boxCol, int boxRow, int col, int row, String val) {

        TextField t = (TextField) getGridNodes(boardGrids[boxCol][boxRow])[col][row];
        t.setText(val);

    }

    /**
     * Function called for testing purposes of locking a field, returning the value of the given textField.
     * @return  Text      The textField containing a variable.
     */
    protected TextField getTextField(int boxCol, int boxRow, int col, int row) {

        return ((TextField) getGridNodes(boardGrids[boxCol][boxRow])[col][row]);

    }

    /**
     * If a textField already has a number when the JSON board is read it is locked from editing.
     * Also used for testing purposes.
     * @param textField TextField from buildGraphics() or testing.
     */
    protected void lockField(TextField textField) {

        textField.setEditable(false);

    }

    public static void main(String[] args) {

        launch();

    }

}