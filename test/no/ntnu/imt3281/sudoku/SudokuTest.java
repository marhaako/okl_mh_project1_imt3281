package no.ntnu.imt3281.sudoku;

import static org.junit.Assert.*;

import de.saxsys.javafx.test.JfxRunner;
import javafx.application.Application;
import javafx.scene.control.TextField;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

@RunWith(JfxRunner.class)
public class SudokuTest {

	@Test
	public void testEmptyConstructor() {
		Sudoku sudoku = new Sudoku();

		assertTrue(sudoku instanceof Sudoku);
	}

	@Test
    public void testInsertNumber() {
	    Sudoku sudoku = new Sudoku();
		int i = 0;
		while(sudoku.board[i][0] != -1) {
			i++;
		}
		sudoku.board[i][0] = 9;
		assertTrue(sudoku.board[i][0] == 9);

    }

    @Test
    public void testBuildSudokuGame() {
		int[][] board = new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS];
		SudokuBuilder builder = new SudokuBuilder();
		board = builder.build(board);
		assertTrue(board != null);
    }

	@Test
	public void testInsertNumberInvalidRow() {
		Sudoku sudoku = new Sudoku();
		sudoku.buildGraphics();
		List<String> numbers = new ArrayList<>();
		Boolean insert = false;

		for(int i = 0; i < sudoku.NO_OF_ROWS; i++) {
			String n = (((TextField) sudoku.getGridNodes(sudoku.boardGrids[i/3][0])[i%3][0])).getText();
			System.out.println(n);
			if (n != null && !n.equals("")) { //Get all numbers in row
				numbers.add(n);
			}
		}
		int i = 0;
		while(!insert){
			String n = (((TextField) sudoku.getGridNodes(sudoku.boardGrids[i/3][0])[i%3][0])).getText(); //Find an available spot in row
			if(n == null || n.equals("")) {
				(((TextField) sudoku.getGridNodes(sudoku.boardGrids[i/3][0])[i%3][0])).setText(numbers.get(0)); //Insert existing number
				insert = true;
			}
			i++;
		}
		assertTrue(!sudoku.checkRowValid(0, 0)); //Should return false, because of insertion of existing number
	}

	@Test
	public void testInsertNumberInvalidColumn() {
		Sudoku sudoku = new Sudoku();
		sudoku.buildGraphics();
		List<String> numbers = new ArrayList<>();
		Boolean insert = false;

		for(int i = 0; i < sudoku.NO_OF_ROWS; i++) {
			String n = (((TextField) sudoku.getGridNodes(sudoku.boardGrids[0][i/3])[0][i%3])).getText();
			System.out.println(n);
			if (n != null && !n.equals("")) { //Get all numbers in column
				numbers.add(n);
			}
		}
		int i = 0;
		while(!insert){
			String n = (((TextField) sudoku.getGridNodes(sudoku.boardGrids[0][i/3])[0][i%3])).getText(); //Finner en ledig plass i kolonnen
			if(n == null || n.equals("")) {
				(((TextField) sudoku.getGridNodes(sudoku.boardGrids[0][i / 3])[0][i % 3])).setText(numbers.get(0)); //Setter inn eksisterende tall
				insert = true;
			}
			i++;
		}
		assertTrue(!sudoku.checkColValid(0, 0)); //Sjekker at denne returnerer false, da det ikke er lov
	}

	@Test
	public void testInsertNumberInvalidBox() {
		Sudoku sudoku = new Sudoku();
		sudoku.buildGraphics();
		List<String> numbers = new ArrayList<>();
		Boolean insert = false;

		for(int i = 0; i < sudoku.NO_OF_ROWS; i++) {
			String n = (((TextField) sudoku.getGridNodes(sudoku.boardGrids[0][i/3])[0][i%3])).getText();
			System.out.println(n);
			if (n != null && !n.equals("")) { //Get all numbers in column
				numbers.add(n);
			}
		}
		int i = 0;
		while(!insert){
			String n = (((TextField) sudoku.getGridNodes(sudoku.boardGrids[0][i/3])[0][i%3])).getText(); //Finner en ledig plass i kolonnen
			if(n == null || n.equals("")) {
				(((TextField) sudoku.getGridNodes(sudoku.boardGrids[0][i / 3])[0][i % 3])).setText(numbers.get(0)); //Setter inn eksisterende tall
				insert = true;
			}
			i++;
		}
		assertTrue(!sudoku.checkColValid(0, 0)); //Sjekker at denne returnerer false, da det ikke er lov
	}

	@Test
	public void testRandomizeNumbers() {
		SudokuBuilder builder = new SudokuBuilder();
		builder.build(new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS]);
		int[][] temp = builder.board;
		int num1 = 0; int num2 = 0;
		builder.randomizeNumbers(1);
		for(int i = 0; i < Sudoku.NO_OF_ROWS; i++) {
			for(int j = 0; j < Sudoku.NO_OF_ROWS; j++) {
				if(temp[i][j] != builder.board[i][j]) {
					num1 = temp[i][j]; num2 = builder.board[i][j];
				}
			}
		}
		for(int i = 0; i < Sudoku.NO_OF_ROWS; i++) {
			for(int j = 0; j < Sudoku.NO_OF_ROWS; j++) {
				if(temp[i][j] != builder.board[i][j]) {
					assertTrue(temp[i][j] == num1);
					assertTrue(builder.board[i][j] == num1);
				}
			}
		}

	}

	@Test
	public void testMirrorRed() {
		SudokuBuilder builder = new SudokuBuilder();
		builder.build(new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS]);
		int[][] temp = builder.board;
		builder.board = builder.mirrorRed();
		for (int col = Sudoku.NO_OF_ROWS - 1; col >= 0; col--) {
			for (int row = Sudoku.NO_OF_ROWS - 1; row >= 0; row--) {
				assertEquals(temp[Sudoku.NO_OF_ROWS - row - 1][Sudoku.NO_OF_ROWS - col - 1], builder.board[col][row]);
			}
		}
	}

	@Test
	public void testMirrorBlue() {
		SudokuBuilder builder = new SudokuBuilder();
		builder.build(new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS]);
		int[][] temp = builder.board;
		builder.board = builder.mirrorBlue();
		for (int col = 0; col < Sudoku.NO_OF_ROWS; col++) {
			for (int row = 0; row < Sudoku.NO_OF_ROWS; row++) {
				assertEquals(temp[row][col], builder.board[col][row]);
			}

		}
	}

	@Test
	public void testMirrorHorizontal() {
		SudokuBuilder builder = new SudokuBuilder();
		builder.build(new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS]);
		int[][] temp = builder.board;
		builder.board = builder.mirrorHorizontally(); //Mirror the board horizontally
		for (int col = Sudoku.NO_OF_ROWS - 1; col >= 0; col--) {
			for (int row = Sudoku.NO_OF_ROWS - 1; row >= 0; row--) {
				assertEquals(temp[Sudoku.NO_OF_ROWS - row - 1][Sudoku.NO_OF_ROWS - col - 1], builder.board[Sudoku.NO_OF_ROWS - row - 1][col]);
			}
		}
	}

	@Test
	public void testMirrorVertical() {
		SudokuBuilder builder = new SudokuBuilder();
		builder.build(new int[Sudoku.NO_OF_ROWS][Sudoku.NO_OF_ROWS]);
		int[][] temp = builder.board;
		builder.board = builder.mirrorVertical(); //Mirror the board horizontally
		for (int col = 0; col < Sudoku.NO_OF_ROWS; col++) {
			for (int row = Sudoku.NO_OF_ROWS - 1; row >= 0; row--) {
				assertEquals(temp[Sudoku.NO_OF_ROWS - row - 1][col], builder.board[row][col]);
			}
		}
	}

	@Test
	public void testGetValue() {
		Sudoku sudoku = new Sudoku();
		sudoku.buildGraphics();
		for(int i = 0; i < Sudoku.NO_OF_ROWS; i++) {
			for(int j = 0; j < Sudoku.NO_OF_ROWS; j++) {
				if(!sudoku.checkEditable(j/3, i/3, j%3, i%3)) { //Check if field actually has a number to get
					assertTrue(Integer.parseInt(sudoku.getNode(j/3, i/3, j%3, i%3)) == sudoku.board[i][j]);
				}
			}
		}
	}

	@Test
	public void testLockedCell() {
		Sudoku sudoku = new Sudoku();
		sudoku.buildGraphics();
		for(int i = 0; i < Sudoku.NO_OF_ROWS; i++) {
			for(int j = 0; j < Sudoku.NO_OF_ROWS; j++) {
				if(sudoku.board[i][j] == -1) { //When cell is -1, it should be editable
					assertTrue(sudoku.checkEditable(j/3, i/3, j%3, i%3));
				} else {	//When cell has number in it, it should not be editable
					assertTrue(!sudoku.checkEditable(j/3, i/3, j%3, i%3));
				}

			}
		}
	}

	/**
	 * Checking that a textField is editable, then locking it, then checking that it is not editable.
	 */
	@Test
	public void testLockField() {
		Sudoku sudoku = new Sudoku();
		sudoku.buildGraphics();
		for(int i = 0; i < Sudoku.NO_OF_ROWS; i++) {
			for(int j = 0; j < Sudoku.NO_OF_ROWS; j++) {
				if(sudoku.board[i][j] == -1) { //When cell is -1, it should be editable
					assertTrue(sudoku.checkEditable(j / 3, i / 3, j % 3, i % 3));
					sudoku.lockField(sudoku.getTextField(j / 3, i / 3, j % 3, i % 3));
					assertFalse(sudoku.checkEditable(j / 3, i / 3, j % 3, i % 3));
;				}
			}
		}
	}

}
